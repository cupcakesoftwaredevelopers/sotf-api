package auth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Paulien on 26-1-2016.
 */
public class PasswordUtilsTest {
    String password = "thisIsApassWord!";
    String password2 = "aPassword";

    @org.junit.Test
    public void testHashPassword() throws Exception {
        String salt = PasswordUtils.generateSalt();

        String hashed1 = PasswordUtils.hashPassword(password, salt);
        String hashed2 = PasswordUtils.hashPassword(password, salt);

        String hashed3 = PasswordUtils.hashPassword(password2, salt);

        assertEquals(hashed1, hashed2);
        assertNotEquals(hashed1, hashed3);
    }
}