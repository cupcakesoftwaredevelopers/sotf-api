package hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


/**
 * Created by Paulien on 21-1-2016.
 */
public class HibernateUtil {
    private static SessionFactory sessionFactory = null;

    static
    {
        init();
    }

    public static SessionFactory getSessionFactory()
    {
        if( sessionFactory == null ){
            init();
        }
        return sessionFactory;
    }

    private static void init() {
        try
        {
            // Create the SessionFactory from hibernate.cfg.xml
            StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                    .configure()
                    .build();

            Metadata metadata = new MetadataSources( standardRegistry )
                    // You can add more entity classes here like above
                    .addResource("Gebruiker.hbm.xml")
                    .addResource("Boodschappenlijst.hbm.xml")
                    .addResource("Winkelwagen.hbm.xml")
                    .addResource("Product.hbm.xml")
                    .addResource("Boodschappenlijstproduct.hbm.xml")
                    .addResource("Winkelwagenproduct.hbm.xml")
                    .getMetadataBuilder()
                    .applyImplicitNamingStrategy( ImplicitNamingStrategyJpaCompliantImpl.INSTANCE )
                    .build();

            sessionFactory = metadata.getSessionFactoryBuilder().build();

        }
        catch (Throwable ex)
        {
            // Make sure you log the exception, as it might be swallowed
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
}
