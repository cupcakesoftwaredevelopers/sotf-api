package validator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paulien on 22-1-2016.
 */
public abstract class AbstractValidator<T> {
    public static final String ALL_FIELDS_EMPTY = "ALL_FIELDS_EMPTY";
    public static final String FIELD_EXISTS = "FIELD_EXISTS";
    public static final String FIELD_EMPTY = "FIELD_EMPTY";
    public static final String FIELD_NOT_VALID = "FIELD_NOT_VALID";

    private Map<String, String> errorMap;

    public AbstractValidator() {
        errorMap = new HashMap<>();
    }

    public Map<String, String> getErrorMap() {
        return errorMap;
    }

    public String getErrorCode(String key) {
        if ( errorMap.containsKey(key) ){
            return errorMap.get(key);
        }
        return null;
    }

    /**
     * Method to add Errorcode for an param
     * @param key Param name
     * @param value Error code
     */
    protected void putErrorCode(String key, String value){
        errorMap.put(key,value);
    }

    abstract boolean validate(T object);
}
