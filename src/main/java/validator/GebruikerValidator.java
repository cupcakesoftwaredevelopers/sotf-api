package validator;

import com.mysql.jdbc.StringUtils;
import model.Gebruiker;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.IntegerValidator;

/**
 * Created by Paulien on 22-1-2016.
 */
public class GebruikerValidator extends AbstractValidator<Gebruiker> {


    @Override
    public boolean validate(Gebruiker gebruiker) {
        boolean valid = true;

        if( gebruiker ==  null){
            valid = false;
            putErrorCode("error", ALL_FIELDS_EMPTY);
        }else{
            if(StringUtils.isNullOrEmpty(gebruiker.getAchternaam())){
                valid = false;
                putErrorCode("last_name", FIELD_EMPTY);
            }
            if(StringUtils.isNullOrEmpty(gebruiker.getVoornaam())){
                valid = false;
                putErrorCode("first_name", FIELD_EMPTY);
            }
            if(StringUtils.isNullOrEmpty(gebruiker.getGebruikernaam())){
                valid = false;
                putErrorCode("username", FIELD_EMPTY);
            }else{
                if( ! EmailValidator.getInstance().isValid(gebruiker.getGebruikernaam())){
                    valid = false;
                    putErrorCode("username", FIELD_NOT_VALID);
                }
            }
            if(StringUtils.isNullOrEmpty(gebruiker.getWachtwoord())){
                valid = false;
                putErrorCode("password", FIELD_EMPTY);
            }
            if(StringUtils.isNullOrEmpty(gebruiker.getStraat())){
                valid = false;
                putErrorCode("street", FIELD_EMPTY);
            }
            if(StringUtils.isNullOrEmpty(String.valueOf(gebruiker.getHuisnummer()))){
                valid = false;
                putErrorCode("house_number", FIELD_EMPTY);
            }else{
                if( ! IntegerValidator.getInstance().minValue(gebruiker.getHuisnummer(), 1) ){
                    valid = false;
                    putErrorCode("house_number", FIELD_NOT_VALID);
                }
            }
            if(StringUtils.isNullOrEmpty(String.valueOf(gebruiker.getPostcode()))){
                valid = false;
                putErrorCode("postal_code", FIELD_EMPTY);
            }else{
                String postalCodePattern = "[1-9][0-9]{3}\\s?[a-zA-Z]{2}";
                if(!gebruiker.getPostcode().matches(postalCodePattern)){
                    valid = false;
                    putErrorCode("postal_code", FIELD_NOT_VALID);
                }
            }
            if(StringUtils.isNullOrEmpty(String.valueOf(gebruiker.getWoonplaats()))){
                valid = false;
                putErrorCode("city", FIELD_EMPTY);
            }
        }
        return valid;
    }
}
