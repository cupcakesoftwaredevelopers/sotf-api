import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Paulien on 13-1-2016.
 */
public class Main {
    public final static java.util.logging.Logger LOG = java.util.logging.Logger.getLogger("VertxHttpServer");
    private static Vertx vertx;

    public static void main(String[] args){
        VertxOptions options = new VertxOptions();
        options.setBlockedThreadCheckInterval(1000*60*60);

        vertx = Vertx.vertx(options);
        ApiServer apiServer = new ApiServer();
        vertx.deployVerticle(apiServer);

        startConsoleListener();
    }

    private static void startConsoleListener(){
        new Thread(() -> {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String line = "";
            try {
                while ( (line = in.readLine()) != null) {
                    if(line.equalsIgnoreCase("qqq")){
                        vertx.close();
                        break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

        }).start();
    }
}
