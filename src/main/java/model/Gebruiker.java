package model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Gebruiker implements Serializable {
    private Integer id;
    private String voornaam;
    private String achternaam;
    private String gebruikernaam;
    private String wachtwoord;
    private String salt;
    private String straat;
    private Integer huisnummer;
    private String toevoeging;
    private String postcode;
    private String woonplaats;
    private Boodschappenlijst boodschappenlijst;
    private List<Winkelwagen> facturen;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getGebruikernaam() {
        return gebruikernaam;
    }

    public void setGebruikernaam(String gebruikernaam) {
        this.gebruikernaam = gebruikernaam;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public Integer getHuisnummer() {
        return huisnummer;
    }

    public void setHuisnummer(Integer huisnummer) {
        this.huisnummer = huisnummer;
    }

    public String getToevoeging() {
        return toevoeging;
    }

    public void setToevoeging(String toevoeging) {
        this.toevoeging = toevoeging;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gebruiker gebruiker = (Gebruiker) o;

        if (id != null ? !id.equals(gebruiker.id) : gebruiker.id != null) return false;
        if (voornaam != null ? !voornaam.equals(gebruiker.voornaam) : gebruiker.voornaam != null) return false;
        if (achternaam != null ? !achternaam.equals(gebruiker.achternaam) : gebruiker.achternaam != null) return false;
        if (gebruikernaam != null ? !gebruikernaam.equals(gebruiker.gebruikernaam) : gebruiker.gebruikernaam != null)
            return false;
        if (wachtwoord != null ? !wachtwoord.equals(gebruiker.wachtwoord) : gebruiker.wachtwoord != null) return false;
        if (straat != null ? !straat.equals(gebruiker.straat) : gebruiker.straat != null) return false;
        if (huisnummer != null ? !huisnummer.equals(gebruiker.huisnummer) : gebruiker.huisnummer != null) return false;
        if (toevoeging != null ? !toevoeging.equals(gebruiker.toevoeging) : gebruiker.toevoeging != null) return false;
        if (postcode != null ? !postcode.equals(gebruiker.postcode) : gebruiker.postcode != null) return false;
        if (woonplaats != null ? !woonplaats.equals(gebruiker.woonplaats) : gebruiker.woonplaats != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (voornaam != null ? voornaam.hashCode() : 0);
        result = 31 * result + (achternaam != null ? achternaam.hashCode() : 0);
        result = 31 * result + (gebruikernaam != null ? gebruikernaam.hashCode() : 0);
        result = 31 * result + (wachtwoord != null ? wachtwoord.hashCode() : 0);
        result = 31 * result + (straat != null ? straat.hashCode() : 0);
        result = 31 * result + (huisnummer != null ? huisnummer.hashCode() : 0);
        result = 31 * result + (toevoeging != null ? toevoeging.hashCode() : 0);
        result = 31 * result + (postcode != null ? postcode.hashCode() : 0);
        result = 31 * result + (woonplaats != null ? woonplaats.hashCode() : 0);
        return result;
    }

    public Boodschappenlijst getBoodschappenlijst() {
        return boodschappenlijst;
    }

    public void setBoodschappenlijst(Boodschappenlijst boodschappenlijst) {
        this.boodschappenlijst = boodschappenlijst;
    }

    public List<Winkelwagen> getFacturen() {
        return facturen;
    }

    public void setFacturen(List<Winkelwagen> facturen) {
        this.facturen = facturen;
    }
}
