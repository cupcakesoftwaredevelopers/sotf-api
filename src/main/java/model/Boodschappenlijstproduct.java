package model;

import java.io.Serializable;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Boodschappenlijstproduct implements Serializable{
    private Integer id;
    private Integer aantal;
    private Boodschappenlijst boodschappenlijst;
    private Product product;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAantal() {
        return aantal;
    }

    public void setAantal(Integer aantal) {
        this.aantal = aantal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Boodschappenlijstproduct that = (Boodschappenlijstproduct) o;

        if (aantal != null ? !aantal.equals(that.aantal) : that.aantal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return aantal != null ? aantal.hashCode() : 0;
    }

    public Boodschappenlijst getBoodschappenlijst() {
        return boodschappenlijst;
    }

    public void setBoodschappenlijst(Boodschappenlijst boodschappenlijst) {
        this.boodschappenlijst = boodschappenlijst;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
