package model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Winkelwagenproduct implements Serializable {
    private Integer id;
    private BigDecimal prijs;
    private Integer aantal;
    private Product product;
    private Winkelwagen winkelwagen;
    private boolean isNew = false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPrijs() {
        return prijs;
    }

    public void setPrijs(BigDecimal prijs) {
        this.prijs = prijs;
    }

    public Integer getAantal() {
        return aantal;
    }

    public void setAantal(Integer aantal) {
        this.aantal = aantal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Winkelwagenproduct that = (Winkelwagenproduct) o;

        if (prijs != null ? !prijs.equals(that.prijs) : that.prijs != null) return false;
        if (aantal != null ? !aantal.equals(that.aantal) : that.aantal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = prijs != null ? prijs.hashCode() : 0;
        result = 31 * result + (aantal != null ? aantal.hashCode() : 0);
        return result;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Winkelwagen getWinkelwagen() {
        return winkelwagen;
    }

    public void setWinkelwagen(Winkelwagen winkelwagen) {
        this.winkelwagen = winkelwagen;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isNew() {
        return isNew;
    }
}
