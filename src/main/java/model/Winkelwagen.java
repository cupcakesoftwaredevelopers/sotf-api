package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Winkelwagen implements Serializable {
    private Integer id;
    private Date datum;
    private Integer betaald;
    private Gebruiker gebruiker;
    private Set<Winkelwagenproduct> producten;
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Integer getBetaald() {
        return betaald;
    }

    public void setBetaald(Integer betaald) {
        this.betaald = betaald;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Winkelwagen that = (Winkelwagen) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (datum != null ? !datum.equals(that.datum) : that.datum != null) return false;
        if (betaald != null ? !betaald.equals(that.betaald) : that.betaald != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (datum != null ? datum.hashCode() : 0);
        result = 31 * result + (betaald != null ? betaald.hashCode() : 0);
        return result;
    }

    public Gebruiker getGebruiker() {
        return gebruiker;
    }

    public void setGebruiker(Gebruiker gebruiker) {
        this.gebruiker = gebruiker;
    }

    public Set<Winkelwagenproduct> getProducten() {
        return producten;
    }

    public void setProducten(Set<Winkelwagenproduct> producten) {
        this.producten = producten;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
