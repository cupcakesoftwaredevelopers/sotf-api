package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Boodschappenlijst implements Serializable{


    private Integer id;
    private String naam;
    private String omschrijving;
    private Date datum;
    private Gebruiker gebruiker;
    private Set<Boodschappenlijstproduct> boodschappenProducten;
/*    private Integer gebruikerId;

    public Integer getGebruikerId() {
        return gebruikerId;
    }

    public void setGebruikerId(Integer gebruikerId) {
        this.gebruikerId = gebruikerId;
    }*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getOmschrijving() {
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        this.omschrijving = omschrijving;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Boodschappenlijst that = (Boodschappenlijst) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (naam != null ? !naam.equals(that.naam) : that.naam != null) return false;
        if (omschrijving != null ? !omschrijving.equals(that.omschrijving) : that.omschrijving != null) return false;
        if (datum != null ? !datum.equals(that.datum) : that.datum != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (naam != null ? naam.hashCode() : 0);
        result = 31 * result + (omschrijving != null ? omschrijving.hashCode() : 0);
        result = 31 * result + (datum != null ? datum.hashCode() : 0);
        return result;
    }

    public Gebruiker getGebruiker() {
        return gebruiker;
    }

    public void setGebruiker(Gebruiker gebruiker) {
        this.gebruiker = gebruiker;
    }

    public Set<Boodschappenlijstproduct> getBoodschappenProducten() {
        return boodschappenProducten;
    }

    public void setBoodschappenProducten(Set<Boodschappenlijstproduct> boodschappenProducten) {
        this.boodschappenProducten = boodschappenProducten;
    }
}
