package model;

import io.vertx.core.json.JsonObject;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Paulien on 21-1-2016.
 */
public class Product implements Serializable {
    public static final String RES_PRODUCT_ID = "barcode";
    public static final String RES_PRODUCT_NAME = "name";
    public static final String RES_PRODUCT_EENHEID = "detail";
    public static final String RES_PRODUCT_BRAND = "brand";
    public static final String RES_PRODUCT_PRICE = "price";

    private Long id;
    private String naam;
    private String merk;
    private String eenheid;
    private BigDecimal prijs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getEenheid() {
        return eenheid;
    }

    public void setEenheid(String eenheid) {
        this.eenheid = eenheid;
    }

    public BigDecimal getPrijs() {
        return prijs;
    }

    public void setPrijs(BigDecimal prijs) {
        this.prijs = prijs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (id != null ? !id.equals(product.id) : product.id != null) return false;
        if (naam != null ? !naam.equals(product.naam) : product.naam != null) return false;
        if (merk != null ? !merk.equals(product.merk) : product.merk != null) return false;
        if (eenheid != null ? !eenheid.equals(product.eenheid) : product.eenheid != null) return false;
        if (prijs != null ? !prijs.equals(product.prijs) : product.prijs != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (naam != null ? naam.hashCode() : 0);
        result = 31 * result + (merk != null ? merk.hashCode() : 0);
        result = 31 * result + (eenheid != null ? eenheid.hashCode() : 0);
        result = 31 * result + (prijs != null ? prijs.hashCode() : 0);
        return result;
    }

    public JsonObject toJson(){
        JsonObject productJson = new JsonObject();
        productJson.put(RES_PRODUCT_ID, getId());
        productJson.put(RES_PRODUCT_NAME, getNaam());
        productJson.put(RES_PRODUCT_PRICE, getPrijs().doubleValue());
        productJson.put(RES_PRODUCT_EENHEID, getEenheid());
        productJson.put(RES_PRODUCT_BRAND, getMerk());
        return productJson;
    }
}
