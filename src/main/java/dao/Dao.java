package dao;

import hibernate.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;
import java.util.Map;

/**
 * Created by Paulien on 21-1-2016.
 */

public class Dao {
    /**
     * Create object
     *
     * @param obj
     * @throws Exception
     */

    public static void create(Object obj) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(obj);
        session.getTransaction().commit();
    }

    /**
     * Update object
     *
     * @param obj
     * @throws Exception
     */

    public static void update(Object obj) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(obj);
        session.getTransaction().commit();
    }

    /**
     * Delete object
     *
     * @param obj
     * @throws Exception
     */

    public static void delete(Object obj) throws Exception {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(obj);
        session.getTransaction().commit();
    }

    public static <T> T get(Class<T> clazz, int id) {
        return get(clazz, id, false);
    }

    public static <T> T get(Class<T> clazz, long id) {
        return get(clazz, id, false);
    }

    /**
     * Find by id
     *
     * @param clazz
     * @param id
     * @param <T>
     * @return
     */

    public static <T> T get(Class<T> clazz, int id, boolean close) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        T t = session.get(clazz, id);
        if( close) session.close();
        return t;
    }

    public static <T> T get(Class<T> clazz, long id, boolean close) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        T t = session.get(clazz, id);
        if( close) session.close();
        return t;
    }

    /**
     * Find by params
     *
     * @param clazz  class to find
     * @param params params to search
     * @return
     */

    public static <T> List<T> get(Class<T> clazz, Map<String, Object> params, boolean close) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(clazz);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
            }
        }
        List<T> list = criteria.list();//ignore
        if( close) session.close();
        return list;
    }

    public static <T> List<T> get(Class<T> clazz, Map<String, Object> params) {
        return get(clazz, params, false);
    }

    /**
     * Find by params
     *
     * @param clazz  class to find
     * @param params params to search
     * @return
     */

    public static <T> T getFirstOrNull(Class<T> clazz, Map<String, Object> params, boolean close) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Criteria criteria = session.createCriteria(clazz);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                criteria.add(Restrictions.eq(entry.getKey(), entry.getValue()));
            }
        }
        List<T> list = criteria.list();//ignore

        if( close) session.close();

        return list.size() > 0 ? list.get(0) : null;
    }

}
