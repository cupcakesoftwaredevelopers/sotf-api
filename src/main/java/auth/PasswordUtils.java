package auth;

import io.vertx.ext.auth.jdbc.impl.JDBCAuthImpl;

import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by Paulien on 26-1-2016.
 */
public class PasswordUtils {

    public static String generateSalt() {
        final Random r = new SecureRandom();
        byte[] salt = new byte[32];
        r.nextBytes(salt);
        return JDBCAuthImpl.bytesToHex(salt);
    }

    public static String hashPassword(String password, String salt){
        return JDBCAuthImpl.computeHash(password, salt, "SHA-512");
    }

}
