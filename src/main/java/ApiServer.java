import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import rest.RouterFactory;

/**
 * Created by Paulien on 13-1-2016.
 */
public class ApiServer extends AbstractVerticle {

    private static final int PORT = 8123; // default to 8080.
    private Router router;

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);

        RouterFactory factory = new RouterFactory(vertx);
        router = factory.create();
    }

    public void start(Future<Void> future) throws Exception {
        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        // Retrieve the port from the configuration,
                        config().getInteger("http.port", PORT),
                        result -> {
                            if (result.succeeded()) {
                                future.complete();
                            } else {
                                future.fail(result.cause());
                            }
                        }
                );

        Main.LOG.info("Webserver started, listening on port: " + PORT);
        Main.LOG.info("To stop the server fill in the command \"qqq\" and press enter.");
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        Main.LOG.info("Webserver stopped");
    }
}
