package rest;

import dao.Dao;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import model.Boodschappenlijst;
import model.Boodschappenlijstproduct;
import model.Gebruiker;
import model.Product;

import java.sql.Date;
import java.util.*;

/**
 * Created by Paulien on 22-1-2016.
 */
public class ShoppingListRoute extends Route {
    private final String route = "shoppinglist";

    private final String RES_GROCERY_ID = "grocery_id";
    private final String RES_GROCERY_NAME = "name";
    private final String RES_GROCERY_DATE = "date";
    private final String RES_GROCERY_DESC = "description";

    private final String RES_PRODUCT_AMOUNT = "amount";


    public ShoppingListRoute(Router router) {
        super(router);
    }

    @Override
    void defineRoute() {

        router.route(SLASH + route + "*").handler(BodyHandler.create());

        // Get shoppinglist
        router.route(HttpMethod.POST, SLASH + route).handler(routingContext -> {
            System.out.println(routingContext.getBody().toString());
            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");

            if( ! checkAuthorization(routingContext, user, token) ) return;

            Boodschappenlijst boodschappenlijst = getExistingOrNewBoodschappenlijst(user);
            JsonObject toReturn = toJsonObject(boodschappenlijst);

            String responseString = toReturn.toString();
            String handler = routingContext.request().getParam("handler");
            if(handler != null) {
                responseString = handler + "(" + responseString + ")";
            }
            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(responseString);

        });

        // Add/Update shoppinglistproduct
        router.route(HttpMethod.POST, SLASH + route+ SLASH + "add").handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");
            JsonObject product = input.getJsonObject("product");

            if( ! checkAuthorization(routingContext, user, token) ) return;

            Boodschappenlijst boodschappenlijst = getExistingOrNewBoodschappenlijst(user);

            Integer amount = product.getInteger("amount");
            Long barcode = product.getLong("barcode");

            Product p = Dao.get(Product.class, barcode);

            Map<String, Object> params = new HashMap<>();
            params.put("boodschappenlijst",boodschappenlijst);
            params.put("product", p);

            Boodschappenlijstproduct bp = Dao.getFirstOrNull(Boodschappenlijstproduct.class, params, true);

            boolean updated = false;
            if( bp != null) {
                bp.setAantal(amount);

                try {
                    Dao.update(bp);
                    updated = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                bp = new Boodschappenlijstproduct();
                bp.setBoodschappenlijst(boodschappenlijst);
                bp.setProduct(p);
                bp.setAantal(amount);

                try {
                    Dao.create(bp);
                    updated = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            JsonObject toReturn = new JsonObject();
            toReturn.put(RES_STATUS, updated ? "success" : "failed");

            String responseString = toReturn.toString();
            String handler = routingContext.request().getParam("handler");
            if(handler != null) {
                responseString = handler + "(" + responseString + ")";
            }
            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(responseString);
        });

        // Delete shoppinglistproduct
        router.route(HttpMethod.POST, SLASH + route+ SLASH + "delete").handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");
            Long barcode = input.getLong("barcode");

            if( ! checkAuthorization(routingContext, user, token) ) return;

            Boodschappenlijst boodschappenlijst = getExistingOrNewBoodschappenlijst(user);
            Product product = Dao.get(Product.class, barcode);

            Map<String, Object> params = new HashMap<>();
            params.put("boodschappenlijst", boodschappenlijst);
            params.put("product", product);

            Boodschappenlijstproduct bp = Dao.getFirstOrNull(Boodschappenlijstproduct.class, params, true);

            boolean deleted = false;
            if( bp != null) {
                try {
                    Dao.delete(bp);
                    deleted = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            JsonObject toReturn = new JsonObject();
            toReturn.put(RES_STATUS, deleted ? "success" : "failed");

            String responseString = toReturn.toString();
            String handler = routingContext.request().getParam("handler");
            if(handler != null) {
                responseString = handler + "(" + responseString + ")";
            }
            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(responseString);
        });
    }

    private Boodschappenlijst getExistingOrNewBoodschappenlijst(Integer id){
        Gebruiker gebruiker = Dao.get(Gebruiker.class, id);

        HashMap<String, Object> params = new HashMap<>();
        params.put("gebruiker", gebruiker);

        List<Boodschappenlijst> found = Dao.get(Boodschappenlijst.class, params);
        Boodschappenlijst boodschappenlijst;
        if (found.size() > 0) {
            boodschappenlijst = found.get(0);
        }else{
            boodschappenlijst = createBoodschappenlijst(gebruiker);
        }
        return boodschappenlijst;
    }

    private Boodschappenlijst createBoodschappenlijst(Gebruiker gebruiker){
        Boodschappenlijst boodschappenlijst = new Boodschappenlijst();
        boodschappenlijst.setGebruiker(gebruiker);
        boodschappenlijst.setDatum(new Date(System.currentTimeMillis()));
        boodschappenlijst.setNaam("Boodschappenlijst");
        boodschappenlijst.setOmschrijving("");// todo is necessary?
        boodschappenlijst.setBoodschappenProducten(new HashSet<>());

        try {
            Dao.create(boodschappenlijst);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return boodschappenlijst;
    }

    private JsonObject toJsonObject(Boodschappenlijst boodschappenlijst){
        JsonObject toReturn = new JsonObject();

        toReturn.put(RES_GROCERY_ID, boodschappenlijst.getId());
        toReturn.put(RES_GROCERY_NAME, boodschappenlijst.getNaam());
        toReturn.put(RES_GROCERY_DESC, boodschappenlijst.getOmschrijving());
        toReturn.put(RES_GROCERY_DATE, boodschappenlijst.getDatum().getTime());

        HashMap<String, Object> params = new HashMap<>();
        params.put("boodschappenlijst", boodschappenlijst);

        List<Boodschappenlijstproduct> boodschappenProducten = Dao.get(Boodschappenlijstproduct.class, params);

        JsonArray products = new JsonArray();
        for (Boodschappenlijstproduct bp : boodschappenProducten) {
            Product product = bp.getProduct();
            Integer aantal = bp.getAantal();

            JsonObject productJson = product.toJson();
            productJson.put(RES_PRODUCT_AMOUNT, aantal);
            products.add(productJson);
        }
        toReturn.put("products", products);

        return toReturn;
    }

}
