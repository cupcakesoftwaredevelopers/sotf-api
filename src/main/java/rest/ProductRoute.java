package rest;

import dao.Dao;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import model.Product;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Paulien on 25-1-2016.
 */
public class ProductRoute  extends Route{
    public static final String route = "product";
    private JsonArray products = null;

    public ProductRoute(Router router) {
        super(router);
    }

    @Override
    void defineRoute() {

        //get products
        router.route(HttpMethod.GET, SLASH + route).handler(routingContext -> {

                if (products == null) {
                    products = new JsonArray();

                    List<Product> found = Dao.get(Product.class, new HashMap<>());
                    for (Product product : found) {
                        products.add(product.toJson());
                    }
                }

            String responseText = "";
            responseText += products.toString();
            String callback = routingContext.request().getParam("handler");
            if(callback != null) {
                responseText = callback + "(" + responseText + ")";
            }
            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(responseText);

        });

        //get product
        router.route(HttpMethod.GET, SLASH + route + SLASH + ":id").handler(routingContext -> {

            String productId = routingContext.request().getParam("id");

            JsonObject toReturn = new JsonObject();
            try {
                Product product = Dao.get(Product.class, Long.valueOf(productId));
                if (product != null) toReturn = product.toJson();
            }catch (NumberFormatException e){
                e.printStackTrace();
            }

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(toReturn.toString());

        });
    }


}
