package rest;

import dao.Dao;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import model.Gebruiker;
import model.Product;
import model.Winkelwagen;
import model.Winkelwagenproduct;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.*;

/**
 * Created by emmap on 25-1-2016.
 */
public class BasketRoute extends Route {

    private static final String route = "basket";

    private final String RES_PAID = "paid";
    private final String RES_CODE = "code";
    private final String RES_DATUM = "date";

    private final String RES_AMOUNT = "amount";
    private final String RES_PRICE = "price";

    public BasketRoute(Router router) {
        super(router);
    }

    @Override
    void defineRoute() {
        router.route(SLASH + route + "*").handler(BodyHandler.create());

        // Add basket
        router.route(HttpMethod.POST, "/" + route).handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");
            JsonObject basket = input.getJsonObject("basket");
            JsonArray products = basket.getJsonArray("products");

            if (!checkAuthorization(routingContext, user, token)) return;

            Gebruiker gebruiker = Dao.get(Gebruiker.class, user);

            Map<String, Object> params = new HashMap<>();
            params.put("gebruiker", gebruiker);
            params.put("betaald", 0);

            boolean isNew = false;
            Winkelwagen winkelwagen = Dao.getFirstOrNull(Winkelwagen.class, params, true);
            if (winkelwagen == null) {

                winkelwagen = new Winkelwagen();
                winkelwagen.setBetaald(0);
                winkelwagen.setCode("");
                winkelwagen.setGebruiker(gebruiker);
                isNew = true;
            }
            Date date = new Date(System.currentTimeMillis());
            winkelwagen.setDatum(date);

            boolean saved = true;
            if(isNew) {
                try {
                    Dao.create(winkelwagen);
                } catch (Exception e) {
                    e.printStackTrace();
                    saved = false;
                }
            }else{
                try {
                    Dao.update(winkelwagen);
                } catch (Exception e) {
                    e.printStackTrace();
                    saved = false;
                }
            }

            if( isNew && saved ) winkelwagen = Dao.getFirstOrNull(Winkelwagen.class, params, true);

            Map<String, Object> existingParams = new HashMap<>();
            existingParams.put("winkelwagen", winkelwagen);
            List<Winkelwagenproduct> toDelete = Dao.get(Winkelwagenproduct.class,  existingParams, true);

            List<Winkelwagenproduct> wwproducten = new ArrayList<>();
            for (int i = 0; i < products.size(); i++) {
                JsonObject jsonObject = products.getJsonObject(i);

                long id = jsonObject.getLong(Product.RES_PRODUCT_ID);
                double sPrice = jsonObject.getDouble(RES_PRICE);
                int sAmount = jsonObject.getInteger(RES_AMOUNT);

                Product product = Dao.get(Product.class, id);

                Map<String, Object> wwpParams = new HashMap<>();
                wwpParams.put("winkelwagen", winkelwagen);
                wwpParams.put("product", product);

                Winkelwagenproduct wwp = Dao.getFirstOrNull(Winkelwagenproduct.class, wwpParams, true);
                if( wwp == null ){
                    wwp = new Winkelwagenproduct();
                    wwp.setProduct(product);
                    wwp.setWinkelwagen(winkelwagen);
                    wwp.setIsNew(true);
                }else{
                    toDelete.remove(wwp);
                }
                wwp.setAantal(sAmount);
                wwp.setPrijs(new BigDecimal(sPrice));

                wwproducten.add(wwp);
            }


            for (Winkelwagenproduct wwp : wwproducten) {
                if( wwp.isNew() ) {
                    try {
                        Dao.create(wwp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        saved = false;
                    }
                }else{
                    try {
                        Dao.update(wwp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        saved = false;
                    }
                }
            }

            for (Winkelwagenproduct wwp : toDelete) {
                if(wwp.getWinkelwagen().equals(winkelwagen)) {
                    try {
                        Dao.delete(wwp);
                    } catch (Exception e) {
                        e.printStackTrace();
                        saved = false;
                    }
                }
            }


            if (products.size() != wwproducten.size()) saved = false;

            JsonObject toReturn = new JsonObject();
            toReturn.put(RES_STATUS, (saved) ? "success" : "failed");
            if (saved) {
                toReturn.put("basket_id", winkelwagen.getId());
            }

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(toReturn.toString());

        });

        router.route(HttpMethod.POST, SLASH + route + "/confirm_payment").handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");

            if (!checkAuthorization(routingContext, user, token)) return;

            int basketId = input.getInteger("basket_id");

            Winkelwagen basket = Dao.get(Winkelwagen.class, basketId, true);

            boolean paid = true;
            try {
                basket.setBetaald(1);
                Dao.update(basket);
            } catch (Exception e) {
                e.printStackTrace();
                paid = false;
            }

            JsonObject json = new JsonObject();
            json.put("payment_succeeded", paid);

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(json.toString());

        });

        router.route(HttpMethod.POST, SLASH + route + "/check").handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String token = input.getString("token");
            Integer user = input.getInteger("user");

            if (!checkAuthorization(routingContext, user, token)) return;

            int basketID = input.getInteger("basket_id");
            Winkelwagen basket = Dao.get(Winkelwagen.class, basketID);

            JsonObject json = new JsonObject();
            json.put("paid", (basket != null && basket.getBetaald() == 1));

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(HttpResponseStatus.OK.code())
                    .putHeader("content-type", "application/json")
                    .end(json.toString());

        });

        router.route(HttpMethod.POST, SLASH + route + SLASH + "history").handler(routingContext -> {

                    JsonObject input = routingContext.getBodyAsJson();
                    String token = input.getString("token");
                    Integer user = input.getInteger("user");

                    if (!checkAuthorization(routingContext, user, token)) return;

                    if (user != null) {
                        Gebruiker gebruiker = Dao.get(Gebruiker.class, Integer.valueOf(user));

                        HashMap<String, Object> params = new HashMap<>();
                        params.put("gebruiker", gebruiker);
                        params.put("betaald", 1);

                        List<Winkelwagen> winkelwagens = Dao.get(Winkelwagen.class, params);

                        JsonObject toReturn = new JsonObject();
                        JsonArray facturen = new JsonArray();

                        for (Winkelwagen winkelwagen : winkelwagens) {
                            JsonObject wagen = new JsonObject();

                            Double sum = 0D;
                            Map<String, Object> wwpParams = new HashMap<>();
                            wwpParams.put("winkelwagen", winkelwagen);

                            List<Winkelwagenproduct> producten = Dao.get(Winkelwagenproduct.class, wwpParams);
                            //Set<Winkelwagenproduct> producten = winkelwagen.getProducten();
                            for (Winkelwagenproduct wwp : producten) {
                                Integer amount = wwp.getAantal();
                                BigDecimal price = wwp.getPrijs();
                                sum += (amount * price.doubleValue());
                            }
                            if (sum > 0D) {
                                wagen.put("id", winkelwagen.getId());
                                wagen.put("code", winkelwagen.getCode());
                                wagen.put("date", winkelwagen.getDatum().getTime());
                                wagen.put("paid", winkelwagen.getBetaald());
                                wagen.put("total", sum);

                                facturen.add(wagen);
                            }
                        }
                        toReturn.put("history", facturen);

                        HttpServerResponse response = routingContext.response();
                        response.setStatusCode(HttpResponseStatus.OK.code())
                                .putHeader("content-type", "application/json")
                                .end(toReturn.toString());
                    } else {
                        HttpServerResponse response = routingContext.response();
                        response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code())
                                .putHeader("content-type", "application/json")
                                .end();
                    }
                }

        );

    }
}
