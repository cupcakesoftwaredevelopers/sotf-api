package rest;

import auth.PasswordUtils;
import dao.Dao;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import model.Gebruiker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paulien on 21-1-2016.
 */
public class LoginRoute extends Route {
    private static final String route = "login";
    private Vertx vertx;

    private final String KEY_USERNAME = "gebruikernaam";

    private final String REQ_USERNAME = "username";
    private final String REQ_PASSWORD = "password";

    private final String ERROR = "ERROR_NOT_VALID";

    public LoginRoute(Vertx vertx, Router router) {
        super(router);
        this.vertx = vertx;
        defineRoute();

    }

    void defineRoute() {
//        JDBCAuth authProvider = AuthManager.getInstance(vertx).getAuthProvider();
//
//        router.route().handler(CookieHandler.create());
//        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
//        router.route().handler(new BodyHandlerImpl());
//        router.route().handler(UserSessionHandler.create(authProvider));
//        router.route("/"+route).handler(FormLoginHandler.create(authProvider));

        router.route(SLASH + route + "*").handler(BodyHandler.create());
        router.route(HttpMethod.POST, SLASH + route).handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String username = input.getString(REQ_USERNAME);
            String password = input.getString(REQ_PASSWORD);

            //JsonObject authInfo = new JsonObject().put(REQ_USERNAME, username).put(REQ_PASSWORD, password);

            int code = HttpResponseStatus.OK.code();
            JsonObject toReturn = new JsonObject();

            Map<String, Object> params = new HashMap<>();
            params.put(KEY_USERNAME, username);

            Gebruiker usr = Dao.getFirstOrNull(Gebruiker.class, params, false);
            if( usr == null ){
                code = HttpResponseStatus.UNAUTHORIZED.code();
                toReturn.put(RES_STATUS, "failed");
            }else{
                String pass = usr.getWachtwoord();
                String inputPass = PasswordUtils.hashPassword(password, usr.getSalt());

                if(pass.equals(inputPass)){
                    // Generate token
                    // todo token generate oauth
                    code =  HttpResponseStatus.OK.code();
                    toReturn.put("user", usr.getId())
                            .put("token", System.currentTimeMillis()+"#"+ usr.getId());
                }else{
                    code = HttpResponseStatus.UNAUTHORIZED.code();
                    toReturn.put(RES_STATUS, "failed");
                }
            }

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(code)
                    .putHeader("content-type", "application/json")
                    .end(toReturn.toString());

        });

    }


}
