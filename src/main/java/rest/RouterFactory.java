package rest;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;

/**
 * Created by Paulien on 21-1-2016.
 */
public class RouterFactory {
    private Vertx vertx;
    private Router router;

    public RouterFactory(Vertx vertx) {
        this.vertx = vertx;
        router = Router.router(vertx);

        initialize();

        router.route("/logout").handler(context -> {
            context.clearUser();
            // Redirect back to the index page
            context.response().putHeader("location", "/").setStatusCode(302).end();
        });

    }

    private void initialize(){
        new LoginRoute(vertx, router);
        new RegisterRoute(router);
        new ShoppingListRoute(router);
        new ProductRoute(router);
        new BasketRoute(router);
    }


    public Router create() {
        return router;
    }
}
