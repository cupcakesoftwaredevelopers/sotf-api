package rest;

import auth.PasswordUtils;
import dao.Dao;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import model.Gebruiker;
import validator.AbstractValidator;
import validator.GebruikerValidator;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paulien on 21-1-2016.
 */
public class RegisterRoute extends Route {
    private static final String REQ_USERNAME = "username";
    private static final String REQ_PASSWORD = "password";
    private static final String REQ_FIRST_NAME = "first_name";
    private static final String REQ_LAST_NAME = "last_name";
    private static final String REQ_STREET = "street";
    private static final String REQ_HOUSE_NUMBER = "house_number";
    private static final String REQ_ADDITION = "addition";
    private static final String REQ_POSTAL_CODE = "postal_code";
    private static final String REQ_CITY = "city";

    private static final String RES_SUCCESS = "SAVED";
    private static final String RES_FAILED = "FAILED";

    private static final String route = "register";

    public RegisterRoute(Router router) {
        super(router);
    }

    @Override
    public void defineRoute() {
        router.route(SLASH + route + "*").handler(BodyHandler.create());
        router.route(HttpMethod.POST, SLASH+ route).handler(routingContext -> {

            JsonObject input = routingContext.getBodyAsJson();
            String gebruikernaam = input.getString(REQ_USERNAME);

            Map<String, Object> params = new HashMap<>();
            params.put("gebruikernaam", gebruikernaam.trim().toLowerCase());

            Gebruiker existing = Dao.getFirstOrNull(Gebruiker.class, params, true);

            JsonObject message = new JsonObject();
            Integer statuscode = HttpResponseStatus.OK.code();

            if(existing != null){
                statuscode = HttpResponseStatus.BAD_REQUEST.code();
                message.put(RES_STATUS, RES_FAILED);
                message.put(REQ_USERNAME, AbstractValidator.FIELD_EXISTS);
            }else {
                Gebruiker gebruiker = parseUser(input);
                GebruikerValidator validator = new GebruikerValidator();

                boolean valid = validator.validate(gebruiker);


                if (valid) {

                    String salt = PasswordUtils.generateSalt();
                    String hashedPass = PasswordUtils.hashPassword(gebruiker.getWachtwoord(), salt);

                    gebruiker.setWachtwoord(hashedPass);
                    gebruiker.setSalt(salt);

                    try {
                        Dao.create(gebruiker);
                        message.put(RES_STATUS, RES_SUCCESS);

                    } catch (Exception e) {
                        e.printStackTrace();
                        statuscode = HttpResponseStatus.BAD_REQUEST.code();
                        message.put(RES_STATUS, RES_FAILED);
                    }
                } else {
                    statuscode = HttpResponseStatus.BAD_REQUEST.code();
                    message.put(RES_STATUS, RES_FAILED);
                    for (Map.Entry<String, String> entry : validator.getErrorMap().entrySet()) {
                        message.put(entry.getKey(), entry.getValue());
                    }
                }
            }

            HttpServerResponse response = routingContext.response();
            response.setStatusCode(statuscode)
                    .putHeader("content-type", "application/json")
                    .end(message.toString());


        });
    }

    private Gebruiker parseUser(JsonObject input){
        if( input == null ) return null;

        String gebruikersnaam = input.getString(REQ_USERNAME).trim().toLowerCase();
        String wachtwoord = input.getString(REQ_PASSWORD).trim();
        String achternaam = input.getString(REQ_LAST_NAME).trim();
        String voornaam = input.getString(REQ_FIRST_NAME).trim();
        String straat = input.getString(REQ_STREET).trim();
        String number = input.getString(REQ_HOUSE_NUMBER).trim();
        String toevoeging = input.getString(REQ_ADDITION).trim();
        String postcode = input.getString(REQ_POSTAL_CODE).trim();
        String plaats = input.getString(REQ_CITY).trim();

        Integer huisnummer = null;

        try{
            huisnummer = Integer.parseInt(number);
        }catch (NumberFormatException e){
            huisnummer = -1;
        }

        Gebruiker gebruiker = new Gebruiker();

        gebruiker.setGebruikernaam(gebruikersnaam);
        gebruiker.setWachtwoord(wachtwoord);
        gebruiker.setAchternaam(achternaam);
        gebruiker.setVoornaam(voornaam);
        gebruiker.setStraat(straat);
        gebruiker.setHuisnummer(huisnummer);
        gebruiker.setToevoeging(toevoeging);
        gebruiker.setPostcode(postcode);
        gebruiker.setWoonplaats(plaats);

        return gebruiker;
    }
}
