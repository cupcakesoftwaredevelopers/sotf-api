package rest;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 * Created by Paulien on 22-1-2016.
 */
public abstract class Route {
     protected static final String SLASH = "/";
     protected static final String RES_STATUS = "status";
     protected Router router;

     public Route(Router router) {
          this.router = router;
          defineRoute();
     }

     abstract void defineRoute();

     protected boolean getAuthorisation(Integer userId, String token){
          // TODO check authorization
          return true;
     }

     protected boolean checkAuthorization(RoutingContext routingContext, Integer user, String token) {
          if( ! getAuthorisation(user, token) ){
               HttpServerResponse response = routingContext.response();
               response.setStatusCode(HttpResponseStatus.UNAUTHORIZED.code())
                       .putHeader("content-type", "application/json")
                       .end();
               return false;
          }
          return true;
     }

     protected boolean hasParams(JsonObject json, String... params){
          if(json == null) return false;
          boolean hasParams = true;
          for (String key : params) {
               if(! json.containsKey(key)){
                    hasParams = false;
                    break;
               }
          }
          return hasParams;

     }

}
